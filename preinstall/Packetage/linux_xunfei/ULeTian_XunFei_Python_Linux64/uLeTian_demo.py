#-*- coding: UTF-8 -*- 
#!/usr/bin/env python
################################################################################
# File:         uLeTian_EAR_demo.py
# RCS:          $Header: $
# Description:  1.Function for text to speech 
#               2.IAT   
# Author:       Xubin Li  (SHENZHEN ULeTian limit corp.)
# Created:      11 13 11:23:04 2014 (Shen Zhen University City) market@uletian.cn
# Modified:     
# Language:     Python
# Package:      ROSNXT
# Status:       Experimental (Do Not Distribute)
# Notice:       ULETIAN_AI=XunFei("XXXXXXX") #你的申请的APPID  
################################################################################
#
# Revisions:
#
################################################################################

PKG = 'ULETIAN_XUNFEI_debug' # this package name
import rospy
import _XunFei
import os
import pyaudio
import wave
import sys
from datetime import datetime	
class Robot_MOUSE():
    def __init__(self,FILENNAME='./ERR/MOUSEERR.wav'):
	self.CHUNK = 1024
	self.FILENNAME = FILENNAME
	    
    def run(self):
    	try:
		self.wf = wave.open(self.FILENNAME, 'rb')	
	except:
		self.wf = wave.open('./ERR/MOUSEERR.wav', 'rb')	
	self.p = pyaudio.PyAudio()
	self.stream = self.p.open(format=self.p.get_format_from_width(self.wf.getsampwidth()),
		        channels=self.wf.getnchannels(),
		        rate=self.wf.getframerate(),
		        output=True)
	self.data = self.wf.readframes(self.CHUNK)
	while self.data != '':
	    self.stream.write(self.data)
	    self.data = self.wf.readframes(self.CHUNK)
	
    def exit(self):
	self.stream.stop_stream()
	self.stream.close()
	self.p.terminate()
	return 1
#======================================================================        
class Robot_EAR():
    def __init__(self,RECORD_SECONDS):
	self.CHUNK = 1024
	self.FORMAT = pyaudio.paInt16
	self.CHANNELS = 1
	self.RATE = 16000
	self.RECORD_SECONDS = RECORD_SECONDS
	self.WAVE_OUTPUT_FILENAME = datetime.now().strftime("%Y-%m-%d_%H_%M_%S")+".wav"
	if sys.platform == 'darwin':
	    self.CHANNELS = 1
    def run(self):
	self.p = pyaudio.PyAudio()
	self.stream = self.p.open(format=self.FORMAT,
		        channels=self.CHANNELS,
		        rate=self.RATE,
		        input=True,
		        frames_per_buffer=self.CHUNK)

	print("* recording")

	self.frames = []

	for i in range(0, int(self.RATE / self.CHUNK * self.RECORD_SECONDS)):
	    self.data = self.stream.read(self.CHUNK)
	    self.frames.append(self.data)

	print("* done recording")   
    def exit(self):
	self.stream.stop_stream()
	self.stream.close()
	self.p.terminate()
	self.wf = wave.open(self.WAVE_OUTPUT_FILENAME, 'wb')
	self.wf.setnchannels(self.CHANNELS)
	self.wf.setsampwidth(self.p.get_sample_size(self.FORMAT))
	self.wf.setframerate(self.RATE)
	self.wf.writeframes(b''.join(self.frames))
	self.wf.close()        
#======================================================================
class XunFei():
    def __init__(self,appId,server_url = "http://dev.voicecloud.cn/index.htm", work_dir = "."):
        self.appId=appId
        self.server_url=server_url
        self.work_dir=work_dir
    
    def login(self):
        return _XunFei.login("appid = %s, server_url = %s, work_dir = %s " % (self.appId,self.server_url,self.work_dir))
    
    def logout(self):
        return _XunFei.logout()
    
    def iat(self,wav_file_path,sub="iat",ssm=1,auf="audio/L16",rate="16000",aue="speex-wb",ent="sms16k",rst="plain",rse="utf8"):
        return _XunFei.iat(wav_file_path,"sub=%s,ssm=%s,auf=%s;rate=%s,aue=%s,ent=%s,rst=%s,rse=%s" % (sub,ssm,auf,rate,aue,ent,rst,rse))

    def text_to_speech(self,src_text,vcn="xiaoyan",aue = "speex-wb",auf="audio/L16",rate="16000",spd = "5",vol = "5",tte = "utf8"):
        return _XunFei.text_to_speech(src_text ,"vcn=%s,aue=%s,auf=%s;rate=%s,spd=%s,vol=%s,tte=%s" % (vcn,aue,auf,rate,spd,vol,tte))

#============================================================================================================================
if __name__ == '__main__':  

    TestText1  = "欢迎使用目前技术领先的深圳市优乐天信息咨询有限公司(www.uletian.cn)，系统软件测试平台系统（www.testerplatform.cn）上线，希望该系统为您的工作和生活提供便利。"
    TestText2  = "谢谢你的使用，我们将会更大的改进在后续的系统设计中。 语音系统模块退出  "
    TestFileName="text_to_speech.wav"
    #8k音频合成参数：aue=speex,auf=audio/L16;rate=8000,其他参数意义参考参数列表
    ret = 0;
    ULETIAN_AI=XunFei("XXXXXXX") #你的申请的APPID
    ULETIAN_AI.login()
    #=========================================================================================================
    ret = ULETIAN_AI.text_to_speech(TestText1);
    ULETIAN_MOUSE=Robot_MOUSE(TestFileName)
    ULETIAN_MOUSE.run()
    ULETIAN_MOUSE.exit()
    #=========================================================================================================	
    ULETIAN_EAR=Robot_EAR(10)
    ULETIAN_EAR.run()
    ULETIAN_EAR.exit()
    os.system('============================================')
    print "Please wait a Moment"                    
    csRetMsg=ULETIAN_AI.iat(ULETIAN_EAR.WAVE_OUTPUT_FILENAME)    
    print csRetMsg
    #=========================================================================================================    
    ret = ULETIAN_AI.text_to_speech(csRetMsg);
    ULETIAN_MOUSE=Robot_MOUSE(TestFileName)
    ULETIAN_MOUSE.run()
    ULETIAN_MOUSE.exit()
    #=========================================================================================================    
    ret = ULETIAN_AI.text_to_speech(TestText2);
    ULETIAN_MOUSE=Robot_MOUSE(TestFileName)
    ULETIAN_MOUSE.run()
    ULETIAN_MOUSE.exit()    
    print "System Close"                      
    ULETIAN_AI.logout()
