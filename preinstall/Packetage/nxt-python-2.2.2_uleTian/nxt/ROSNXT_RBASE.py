#-*- coding: UTF-8 -*-
################################################################################
#
# File:         ROSNXT_CONSTANTS.py
# RCS:          $Header: $
# Description:  Constants for ROSNXT Control
# Author:       Xubin Li
# Created:      11 13 11:23:04 2014 (Shen Zhen University City) lxbin@uletian.cn
# Modified:     
# Language:     Python
# Package:      ROSNXT
# Status:       Experimental (Do Not Distribute)
#
# 
################################################################################
#
# Revisions:
#
################################################################################
#!/usr/bin/env python
PKG = 'NXTROS_ROSE' # this package name
import rospy
import nxt.locator
from nxt.sensor import *
import roslib
import sys,time
from std_msgs.msg import String
def get_touch1sensors(b):
        Value=Touch(b, PORT_1).get_sample()
        Msg='Touch1'+'State:'+str(Value)
	print  Msg
	return Msg
def get_ultrasonicsensors(b):
        DistanceValue=Ultrasonic(b, PORT_2).get_sample()
        noseMsg='ROS2_Nose1_Distance:'+str(DistanceValue)
	return noseMsg
def get_touch2sensors(b):
        Value=Touch(b, PORT_3).get_sample()
        Msg='Touch2'+'State:'+str(Value)
	print  Msg
	return Msg
def set_colorsensors(b,csROSNXTState):
        cs = Color20(b, PORT_4)
        #print "This is the color sensor test make sure that your color sensor is plugged into port 4:"
	print cs.get_color()
	if "拍照" in csROSNXTState:
		print 'RED:'
	        cs.set_light_color(Type.COLORRED)
	if "导航" in csROSNXTState:        
		print 'BLUE:'
		cs.set_light_color(Type.COLORBLUE)
	if "服务" in csROSNXTState:              
		print 'GREEN:'
		cs.set_light_color(Type.COLORGREEN)
	if "故障" in csROSNXTState: 
		print 'FULL:'
		cs.set_light_color(Type.COLORFULL)
		
	if "空闲" in csROSNXTState:        
		print 'OFF:'
	        cs.set_light_color(Type.COLORNONE)
	return csROSNXTState
def get_colorsensors(b):
	cs = Color20(b, PORT_4)
	return cs.get_color()        
class Robot_BASE():
    def __init__(self):
    	self.sock=None
    	self.csROSNXTState="空闲"
    	self.csLastROSNXTState="空闲"

	try:
		self.sock = nxt.locator.find_one_brick(None, 'ROS2', False, None, False,None, None)	    	
	except:
		print 'No ROS2 NXT bricks init No found'		
    	set_colorsensors(self.sock,self.csROSNXTState)
	return

	return 
    def arm1_run(self):
	try:
		self.csMsg=get_touch1sensors(self.sock)
		print self.csMsg
	except:
		print 'arm1_run ROS2 NXT bricks No found'		
    def arm2_run(self):
	try:
		self.csMsg=get_touch2sensors(self.sock)
		print self.csMsg
	except:
		print 'arm2_run ROS2 NXT bricks No found'		

    def nose_run(self):
	try:
		self.csMsg=get_ultrasonicsensors(self.sock)
		print self.csMsg
	except:
		print 'nose_run ROS2 NXT bricks No found'		

    def getState(self):
	return "空闲"			 
    def state_run(self):
	try:
		self.csROSNXTState=self.getState()
		print self.csROSNXTState
		if self.csLastROSNXTState!=self.csROSNXTState:
			print "Change State"
			self.csLastROSNXTState=set_colorsensors(self.sock,self.csROSNXTState)
	except:
		print 'state_run ROS2 NXT bricks No found'		
    def run(self):
    	self.arm1_run()		
    	self.nose_run()    	
    	self.arm2_run()    	
    	self.state_run()
    def __del__(self):
    	self.sock=None    
	return
	
if __name__ == '__main__':      
    a=Robot_BASE()
    for x in xrange(0, 1000):	
    	a.run()
    exit(0)
