#-*- coding: UTF-8 -*-
################################################################################
#
# File:         ROSNXT_CONSTANTS.py
# RCS:          $Header: $
# Description:  Constants for ROSNXT Control
# Author:       Xubin Li
# Created:      11 13 11:23:04 2014 (Shen Zhen University City) lxbin@uletian.cn
# Modified:     
# Language:     Python
# Package:      ROSNXT
# Status:       Experimental (Do Not Distribute)
#
# 
################################################################################
#
# Revisions:
#
################################################################################

'''
Video capture sample.
Sample shows how VideoCapture class can be used to acquire video
frames from a camera of a movie file. Also the sample provides
an example of procedural video generation by an object, mimicking
the VideoCapture interface (see Chess class).

'create_capture' is a convinience function for capture creation,
falling back to procedural video in case of error.

Usage:
    video.py [--shotdir <shot path>] [source0] [source1] ...'

    sourceN is an
     - integer number for camera capture
     - name of video file
     - synth:<params> for procedural video

Synth examples:
    synth:bg=../cpp/lena.jpg:noise=0.1
    synth:class=chess:bg=../cpp/lena.jpg:noise=0.1:size=640x480

Keys:
    ESC    - exit
    SPACE  - save current frame to <shot path> directory

'''
import roslib
import rospy
from std_msgs.msg import String
import threading
import numpy as np
import cv2
from time import clock
from numpy import pi, sin, cos
import common
#import video
from common import anorm2, draw_str
from ROSNXT_TRACK import TrackApp
from ROSNXT_CAMERA import VideoControlAPP
import sys
class Robot_EYE():
	def __init__(self):
		self.video_src=None
		rospy.init_node('Robot_EYE_ROSNXT', anonymous=True)
		self.talker = rospy.Publisher('ROSNXT_EYE', String)
		self.EyeRecInfo=""
		self.SYSRecInfo=""
		self.PIC_list=find_bmp_list()
		self.PicIndex=0
		#print find_bmp_list()
		self.PicMax=len(self.PIC_list)
		return 
	def TrackObject(self,param=0):
		self.video_src = param		
		TrackApp(self.video_src).run()
	def VideoApp(self,param=None):	
		 a=VideoControlAPP()	 
		 a.run()
	def ShowPic(self,param=0):	
		 a=VideoControlAPP()
		 if param==0:
		       if len(self.PIC_list)==0:
			       a.showphotos('./PIC/Index.bmp')
		       else:
			       a.showphotos(self.PIC_list[0])
		 if param==1:
		       self.PicIndex=(self.PicIndex+param)%self.PicMax 
		       a.showphotos(self.PIC_list[self.PicIndex])
		 if param==-1:
		       self.PicIndex=(self.PicIndex+param)%self.PicMax
		       a.showphotos(self.PIC_list[self.PicIndex])
	def callback(self,data):
#    		rospy.loginfo("\nROSNXT_EYE heard: %s",data.data)
		self.EyeRecInfo=data.data
		print self.EyeRecInfo
	
	def system_callback(self,data):
#		rospy.loginfo("\nROSNXT SYSTEM heard: %s",data.data)
		self.SYSRecInfo=data.data
		print self.SYSRecInfo
        
	def ROSNXT_node_listener(self):
		self.listener=rospy.Subscriber('ROSNXT_EYE', String, self.callback)
		rospy.spin()
		
	def ROSNXT_sys_listener(self):
		self.listener=rospy.Subscriber('ROSNXT_SYS_STATUS', String, self.system_callback)
		rospy.spin()
    
	def ROSNXT_EYE_talker(self,data):
		#self.talker = rospy.Publisher('ROSNXT_EYE', String)
	    	self.str = "EYE_%s"%data    	
    		self.talker.publish(self.str)
		#print "ROSNXT_EYE_talker  publish: ",self.str," End"
	
	def ROSNXT_SYS_statuts_talker(self,data):
		global sys_status_pub	
	    	self.str = "SYSTEM_%s"%data    	
    		sys_status_pub.publish(self.str)
		#print "ROSNXT_EYE_talker  publish: ",self.str," End"
        
def thread_main(a):
    global ROSNXT_EYE
    threadname = threading.currentThread().getName()
    if a==0:	
    	print "====================EYE Start SYSTEM Listen Threading ================="
        ROSNXT_SYS_listener(ROSNXT_EYE)
    if a==1:
    	print "====================EYE Wait NODE Listen Threading ======================"
        ROSNXT_NODE_listener(ROSNXT_EYE)
    if a==2:
    	print "====================EYE Start Action Threading ========================="
        ROSNXT_SYS_Action(ROSNXT_EYE)
    if a==3:
    	print "====================EYE Start Action Threading ========================="
        ROSNXT_NODE_Action(ROSNXT_EYE)
    	
    print threadname, a
def ROSNXT_SYS_listener(ROSNXT):
    ROSNXT.ROSNXT_node_listener()
    return 

def ROSNXT_NODE_listener(ROSNXT):
    ROSNXT.ROSNXT_sys_listener()
    return 
def ROSNXT_SYS_Action(ROSNXT):
    #rostopic pub /ROSNXT std_msgs/String "小智退出"
    while(1):
	if "退出" in ROSNXT.SYSRecInfo:	
		ROSNXT.ROSNXT_EYE_talker("退出")
		break
	if "EYE故障" in ROSNXT.SYSRecInfo:
		ROSNXT.ROSNXT_SYS_statuts_talker("EYE故障")
		ROSNXT.SYSRecInfo="EYE故障"
		continue		
    return 
    
def ROSNXT_NODE_Action(ROSNXT):
    global sys_status_pub
    while(1):	
	if "小智退出" in ROSNXT.EyeRecInfo:	
		break		
#===========================ROSNXT_EYE====================================

	if "开始摄像" in ROSNXT.EyeRecInfo:
		#ROSNXT.ROSNXT_EYE_talker("摄像等待")
		ROSNXT.VideoApp()
		continue
		
	if "开始跟踪" in ROSNXT.EyeRecInfo:
		#ROSNXT.ROSNXT_EYE_talker("摄像等待")
		ROSNXT.TrackObject()
		continue

#	if "准备开始拍照" in ROSNXT.EyeRecInfo:
#		ROSNXT.ROSNXT_EYE_talker("准备拍照")	
#		continue
#=============================================================
	if "首张照片" in ROSNXT.EyeRecInfo:	
		#ROSNXT.ROSNXT_EYE_talker("首张照片等待")
		ROSNXT.ShowPic(0)
		continue
	if "下一张照片" in ROSNXT.EyeRecInfo:		
		#ROSNXT.ROSNXT_EYE_talker("下一张等待")
		ROSNXT.ShowPic(1)
		continue
	if "上一张照片" in ROSNXT.EyeRecInfo:		
		#ROSNXT.ROSNXT_EYE_talker("上一张等待")
		ROSNXT.ShowPic(-1)
		continue

    return 

def find_bmp_list():
	import os,re
	re_file=re.compile(r'shot_0_(\d+).bmp')
	file_list=os.listdir(os.getcwd())	
	final_file_list=[]
	for filename in file_list:		
		#print filename
		if  re_file.search(filename):
			final_file_list.append(filename)
	return final_file_list
				
def test():
	a=Robot_EYE()
#	a.TrackObject()
	a.VideoApp()	
	a.ShowPic()	
			
if __name__ == '__main__':
#	find_bmp_list()
#	exit(0)
 	global ROSNXT_EYE,sys_status_pub
 	ROSNXT_EYE=Robot_EYE()
 	sys_status_pub=rospy.Publisher('ROSNXT_SYS_STATUS', String) 	
 	threads = [] 
 	for x in xrange(0, 4):
        	threads.append(threading.Thread(target=thread_main, args=(x,)))
    	for t in threads:
        	t.start()
    	for t in threads:
        	t.join()
 	threads = []
	exit(0)
