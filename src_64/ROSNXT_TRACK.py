#-*- coding: UTF-8 -*-
#/usr/bin/env python
################################################################################
#
# File:         ROSNXT_CONSTANTS.py
# RCS:          $Header: $
# Description:  Constants for ROSNXT Control
# Author:       Xubin Li
# Created:      11 13 11:23:04 2014 (Shen Zhen University City) lxbin@uletian.cn
# Modified:
#     
# Language:     Python 
# SW_Platform:  ROS_HYDRO
# OS_Platform:  Ubuntu_12_04  
# Package:      ROSNXT
# Status:       Experimental (Do Not Distribute)
#
# 
################################################################################
#
# Revisions:
#
################################################################################

'''
Lucas-Kanade tracker
====================

Lucas-Kanade sparse optical flow demo. Uses goodFeaturesToTrack
for track initialization and back-tracking for match verification
between frames.

Usage
-----
lk_track.py [<video_source>]


Keys
----
ESC - exit
跟踪退出
'''
import roslib
import rospy
from std_msgs.msg import String
import threading
import numpy as np
import cv2
import video
from common import anorm2, draw_str
from time import clock

lk_params = dict( winSize  = (15, 15),
                  maxLevel = 2,
                  criteria = (cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 10, 0.03))

feature_params = dict( maxCorners = 500,
                       qualityLevel = 0.3,
                       minDistance = 7,
                       blockSize = 7 )

class TrackApp:
    def __init__(self, video_src=0):
    	rospy.init_node('Robot_EYE_ROSNXT', anonymous=True)
	self.RecInfo=""	

        self.track_len = 10
        self.detect_interval = 5
        self.tracks = []
	self.video_src=video_src
        self.frame_idx = 0

    def run(self):
    	cam = video.create_capture(self.video_src)
        while True:
            ret, frame = cam.read()
            frame_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            vis = frame.copy()

            if len(self.tracks) > 0:
                img0, img1 = self.prev_gray, frame_gray
                p0 = np.float32([tr[-1] for tr in self.tracks]).reshape(-1, 1, 2)
                p1, st, err = cv2.calcOpticalFlowPyrLK(img0, img1, p0, None, **lk_params)
                p0r, st, err = cv2.calcOpticalFlowPyrLK(img1, img0, p1, None, **lk_params)
                d = abs(p0-p0r).reshape(-1, 2).max(-1)
                good = d < 1
                new_tracks = []
                for tr, (x, y), good_flag in zip(self.tracks, p1.reshape(-1, 2), good):
                    if not good_flag:
                        continue
                    tr.append((x, y))
                    if len(tr) > self.track_len:
                        del tr[0]
                    new_tracks.append(tr)
                    cv2.circle(vis, (x, y), 2, (0, 255, 0), -1)
                self.tracks = new_tracks
                cv2.polylines(vis, [np.int32(tr) for tr in self.tracks], False, (0, 255, 0))
                draw_str(vis, (20, 20), 'track count: %d' % len(self.tracks))

            if self.frame_idx % self.detect_interval == 0:
                mask = np.zeros_like(frame_gray)
                mask[:] = 255
                for x, y in [np.int32(tr[-1]) for tr in self.tracks]:
                    cv2.circle(mask, (x, y), 5, 0, -1)
                p = cv2.goodFeaturesToTrack(frame_gray, mask = mask, **feature_params)
                if p is not None:
                    for x, y in np.float32(p).reshape(-1, 2):
                        self.tracks.append([(x, y)])


            self.frame_idx += 1
            self.prev_gray = frame_gray
            cv2.imshow('lk_track', vis)
	    #Get Message of ROSNXT	
	    self.listener=rospy.Subscriber('ROSNXT_EYE', String, self.callback)
	    self.talker = rospy.Publisher('ROSNXT_EYE', String)

            ch = 0xFF & cv2.waitKey(1)
            if ch == 27 or ("EYE_跟踪退出" in self.RecInfo):
		#cv2.destroyAllWindows()	
		self.talker.publish("EYE_完成跟踪")
		break
	cv2.destroyAllWindows()	
    def callback(self,data):
	    #rospy.loginfo(rospy.get_caller_id()+"I heard %s",data.data)
	    self.RecInfo=str(data.data)
    
if __name__ == '__main__':
    a=TrackApp()
    a.run()
	




