#-*- coding: UTF-8 -*-
################################################################################
#
# File:         ROSNXT_CONSTANTS.py
# RCS:          $Header: $
# Description:  Constants for ROSNXT Control
# Author:       Xubin Li
# Created:      11 13 11:23:04 2014 (Shen Zhen University City) lxbin@uletian.cn
# Modified:     
# Language:     Python
# Package:      ROSNXT
# Status:       Experimental (Do Not Distribute)
#
# 
################################################################################
#
# Revisions:
#
################################################################################
_DEBUGLEVEL = 1
_MONITOR_LEVEL = 1
#====================================================================
MOUSEERRSOUND="./ERR/MOUSEERR.wav"
MOUSESOUND ="./text_to_speech.wav"
#====================================================================
EARPIC     ="./PIC/EAR.BMP"
EARCHANNELS = 1
EARRATE = 16000
EARCHUNK = 1024
LOGININID = 8888
#====================================================================
MOUSECHUNK = 1024
#====================================================================
APPID0	   ="5452460e"
APPID1	   ="5452460f"
XUNFEIURL  ="http://dev.voicecloud.cn/index.htm"
CITYCODEURL ='http://61.4.185.48:81/g/'
WEATHERCITYURL  ='http://www.weather.com.cn/data/cityinfo/'
CITYCODE      = "./Weather/city.txt"
LOCALCITYCODE = 101280601
LOCALCITYNAME = "深圳"
CITYNUM =	2586
#====================================================================
# Turning ROSNXT accelerator readings from g's to m/sec^2:
EARTH_GRAVITY = 9.80665             # m/sec^2

#======================================================================
# iat parameters
ROSNXTSUB  = "iat"
ROSNXTssm  = 1
ROSNXTauf  = "audio/L16"
ROSNXTrate = "16000"
ROSNXTaue  = "speex-wb"
ROSNXTent  = "sms16k"
ROSNXTrst  = "plain"
ROSNXTrse  = "utf8"

# texttospeech parameters      
ROSNXTvcn  = "xiaoyan" 
ROSNXTaue  = "speex-wb"
ROSNXTauf  = "audio/L16"
ROSNXTrate = "16000"
ROSNXTspd  = "5"
ROSNXTvol  = "5"
ROSNXTtte  = "utf8"
#======================================================================
TIMEFORTHLONG=6
BASEFORTHPOWER=80
#======================================================================
TIMETURNLONG=3
BASETURNPOWER=60
#======================================================================
MINDISTANCE=10
MINLOWCOUNT=10
