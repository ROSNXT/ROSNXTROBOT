#-*- coding: UTF-8 -*-
################################################################################
#
# File:         ROSNXT_CONSTANTS.py
# RCS:          $Header: $
# Description:  Constants for ROSNXT Control
# Author:       Xubin Li
# Created:      11 13 11:23:04 2014 (Shen Zhen University City) lxbin@uletian.cn
# Modified:     
# Language:     Python
# Package:      ROSNXT
# Status:       Experimental (Do Not Distribute)
#
# 
################################################################################
#
# Revisions:
#
################################################################################
#!/usr/bin/env python
PKG = 'NXTROS_XUNFEI_EAR' # this package name
import roslib
import rospy
from std_msgs.msg import String
import threading

import time
import pyaudio
import wave
from datetime import datetime
from ROSNXT_CONSTANTS import EARCHANNELS,EARCHUNK,EARRATE,APPID0,XUNFEIURL,LOGININID
from ROSNXT_XUNFEI import * 

class Robot_EAR():
    def __init__(self,RECORD_SECONDS=5):
#=================================Roboert======================================
	rospy.init_node('Robot_EAR_ROSNXT', anonymous=True)
	self.talker = rospy.Publisher('ROSNXT', String)
	self.RecInfo=""
	self.SYSRecInfo=""
	self.WeatherRecInfo=""
	self.EyeRecInfo=""	
	self.MouseRecInfo=""	
	self.BaseRecInfo=""	

#=======================================================================
	self.CHUNK = EARCHUNK
	self.FORMAT = pyaudio.paInt16
	self.CHANNELS = EARCHANNELS
	self.RATE = EARRATE
	self.RECORD_SECONDS = RECORD_SECONDS
	self.WAVE_OUTPUT_FILENAME = datetime.now().strftime("%Y-%m-%d_%H_%M_%S")+".wav"
	if RECORD_SECONDS!=LOGININID:
		self.robotear=ROSNXT_XUNFEI(APPID0)
		ret=self.robotear.login()
		if ret==-1:
			print "迅飞服务器链接失败"
			self.status=False
		else:
			self.status=True

    def run(self):
	print("\n====================开始录音===========================\n")
	self.p = pyaudio.PyAudio()
	self.stream = self.p.open(format=self.FORMAT,
		        channels=self.CHANNELS,
		        rate=self.RATE,
		        input=True,
		        frames_per_buffer=self.CHUNK)
	print("\n====================录音中===========================\n")	
	self.frames = []
	for i in range(0, int(self.RATE / self.CHUNK * self.RECORD_SECONDS)):
	    self.data = self.stream.read(self.CHUNK)
	    self.frames.append(self.data)
	print("\n====================录音中结束===========================\n")	
	   
    def listen(self):
    	if self.status==True:
		self.run()
		self.rec_exit()
		print  self.WAVE_OUTPUT_FILENAME		
		self.ListenMsg=self.robotear.iat(self.WAVE_OUTPUT_FILENAME)
#		self.ListenMsg=self.robotear.iat('./text_to_speech.wav')		
		self.robotear.logout()
#		print"\n====NXT_EAR**********声音识别：*****",self.ListenMsg,"**********\n"
		if self.ListenMsg==0:
			self.status=False		
			print"\n===============声音识别失败===================\n"
			return "声音识别失败"
		else:
			print"\nNXT_listen:********声音识别：*****",self.ListenMsg,"**********\n"
			return self.ListenMsg
	else:
		return self.status		
					

    def rec_exit(self):
	self.stream.stop_stream()
	self.stream.close()
	self.p.terminate()
	self.wf = wave.open(self.WAVE_OUTPUT_FILENAME, 'wb')
	self.wf.setnchannels(self.CHANNELS)
	self.wf.setsampwidth(self.p.get_sample_size(self.FORMAT))
	self.wf.setframerate(self.RATE)
	self.wf.writeframes(b''.join(self.frames))
	self.wf.close()
	print("* end recording") 
#=================================Roboert======================================	
    def callback(self,data):
#    	rospy.loginfo("\nROSNXT heard: %s",data.data)
	self.RecInfo=data.data
	print self.RecInfo
    def mouse_callback(self,data):
#    	rospy.loginfo("\nROSNXT_MOUSE heard: %s",data.data)
	self.MouseRecInfo=data.data
	print self.RecInfo
    def eye_callback(self,data):
#    	rospy.loginfo("\nROSNXT_EYE heard: %s",data.data)
	self.EyeRecInfo=data.data
	print self.RecInfo
    def weather_callback(self,data):
#    	rospy.loginfo("\nROSNXT_WEATHER heard: %s",data.data)
	self.WeatherRecInfo=data.data
	print self.RecInfo
    def base_callback(self,data):
#    	rospy.loginfo("\nROSNXT_BASE heard: %s",data.data)
	self.BaseRecInfo=data.data
	print self.RecInfo	
    def system_callback(self,data):
#    	rospy.loginfo("\nROSNXT SYSTEM heard: %s",data.data)
	self.SYSRecInfo=data.data
	print self.SYSRecInfo
        
    def ROSNXT_node_listener(self):
	self.listener=rospy.Subscriber('ROSNXT', String, self.callback)
	self.mouse_listener=rospy.Subscriber('ROSNXT_MOUSE', String, self.mouse_callback)
	self.eye_listener=rospy.Subscriber('ROSNXT_EYE', String, self.eye_callback)
	self.weather_listener=rospy.Subscriber('ROSNXT_WEATHER', String, self.weather_callback)
	self.base_listener=rospy.Subscriber('ROSNXT_BASE', String, self.base_callback)
	
	# in ROS, nodes are unique named. If two nodes with the same
	# node are launched, the previous one is kicked off. The 
	# anonymous=True flag means that rospy will choose a unique
	# name for our 'talker' node so that multiple talkers can
	# run simultaenously.

	# spin() simply keeps python from exiting until this node is stopped
	rospy.spin()
    def ROSNXT_sys_listener(self):
	self.sys_listener=rospy.Subscriber('ROSNXT_SYS_STATUS', String, self.system_callback)
	# in ROS, nodes are unique named. If two nodes with the same
	# node are launched, the previous one is kicked off. The 
	# anonymous=True flag means that rospy will choose a unique
	# name for our 'talker' node so that multiple talkers can
	# run simultaenously.
	# spin() simply keeps python from exiting until this node is stopped
	rospy.spin()
    
    def ROSNXT_EAR_talker(self,data):
    	self.str = "EAR_%s"%data    	
    	self.talker.publish(self.str)	
    def ROSNXT_SYS_statuts_talker(self,data):
    	global sys_status_pub	
    	self.str = "SYSTEM_%s"%data    	
    	sys_status_pub.publish(self.str)        
def thread_main(a):
    global ROSNXT_EAR
    threadname = threading.currentThread().getName()
    if a==0:	
    	print "=============ROS_EAR=======Start SYSTEM Listen Threading ================="
        ROSNXT_SYS_listener(ROSNXT_EAR)
    if a==1:
    	print "=============ROS_EAR=======Wait NODE Listen Threading ===================="
        ROSNXT_NODE_listener(ROSNXT_EAR)
    if a==2:
    	print "=============ROS_EAR=======Start Action Threading ========================="
        ROSNXT_SYS_Action(ROSNXT_EAR)
    if a==3:
    	print "=============ROS_EAR=======Start Action Threading ========================="
        ROSNXT_NODE_Action(ROSNXT_EAR)
    	
    print threadname, a
def ROSNXT_SYS_listener(ROSNXT):
    ROSNXT.ROSNXT_sys_listener()
    return 

def ROSNXT_NODE_listener(ROSNXT):

    ROSNXT.ROSNXT_node_listener()    
    return 
def ROSNXT_SYS_Action(ROSNXT):
   #rostopic pub /ROSNXT std_msgs/String "小智退出"
   ROSNXT.ROSNXT_SYS_statuts_talker("EAR工作")
   Idle_list0={"EAR空闲":1,"MOUSE空闲":1,"EYE空闲":1,"BASE空闲":1,"WEATHER空闲":1}
   Idle_list={"EAR空闲":0,"MOUSE空闲":0,"EYE空闲":1,"BASE空闲":1,"WEATHER空闲":1}
   while(1):   	
	if "小智退出" in ROSNXT.SYSRecInfo:
		ROSNXT.ROSNXT_EAR_talker("退出")
		break		
	if "故障" in ROSNXT.SYSRecInfo:
		continue
		ROSNXT.ROSNXT_SYS_statuts_talker("故障")
		ROSNXT.SYSRecInfo="故障"
		continue
	if ("EAR工作" in ROSNXT.SYSRecInfo): 
		Idle_list["EAR空闲"]=0
		continue	
	if ("MOUSE工作" in ROSNXT.SYSRecInfo): 
		Idle_list["MOUSE空闲"]=0
		continue
	if ("EYE工作" in ROSNXT.SYSRecInfo):
		Idle_list["EYE空闲"]=0
		continue		
	if ("BASE工作" in ROSNXT.SYSRecInfo):
		Idle_list["BASE空闲"]=0
		continue		
	if ("WEATHER工作" in ROSNXT.SYSRecInfo):
		Idle_list["WEATHER空闲"]=0
		continue		
	if ("EAR空闲" in ROSNXT.SYSRecInfo): 
		Idle_list["EAR空闲"]=1	
	if ("MOUSE空闲" in ROSNXT.SYSRecInfo): 
		Idle_list["EAR空闲"]=1
	if ("EYE空闲" in ROSNXT.SYSRecInfo):
		Idle_list["EAR空闲"]=1
	if ("BASE空闲" in ROSNXT.SYSRecInfo):
		Idle_list["EAR空闲"]=1
	if ("WEATHER空闲" in ROSNXT.SYSRecInfo):
		Idle_list["EAR空闲"]=1
	if Idle_list.values()==Idle_list0.values():
		ROSNXT.ROSNXT_SYS_statuts_talker("空闲")
		time.sleep(10)
		#ROSNXT.SYSRecInfo="空闲"
		continue			
   return 

def Info_base_syn(ROSNXTA,ExpectInfo,Timout):
    timeout_t = time.time() + Timout #10 seconds
    while  time.time() < timeout_t:
        if ExpectInfo in ROSNXTA.BaseRecInfo:        
		return True            
	time.sleep(1)
    if time.time() > timeout_t:
        return False
	
def Info_eye_syn(ROSNXTA,ExpectInfo,Timout):
    timeout_t = time.time() + Timout #10 seconds
    while  time.time() < timeout_t:
        if ExpectInfo in ROSNXTA.EyeRecInfo:        
		return True            
	time.sleep(2)
    if time.time() > timeout_t:
        return False

def Info_weather_syn(ROSNXTA,ExpectInfo,Timout):
    timeout_t = time.time() + Timout #10 seconds
    while  time.time() < timeout_t:
        if ExpectInfo in ROSNXTA.WeatherRecInfo:        
		return True            
	time.sleep(2)
    if time.time() > timeout_t:
        return False

def Info_mouse_syn(ROSNXTA,ExpectInfo,Timout):
    timeout_t = time.time() + Timout #10 seconds
    while  time.time() < timeout_t:
        if ExpectInfo in ROSNXTA.MouseRecInfo:        
		return True            
	time.sleep(2)		
    if time.time() > timeout_t:
        return False
        
def ROSNXT_BASE_ACTION(ROSNXTBASE):
    	global mouse_pub,base_pub,ROSNXT_EAR 
    	while(1):
		mouse_pub.publish("MOUSE_语音导航提示")
		if not Info_mouse_syn(ROSNXTBASE,"导航控制提示结束",60):
			ROSNXTBASE.ROSNXT_SYS_statuts_talker("MOUSE故障")
			break
		print "\n=============语音导航提示LISTEN================================\n"			
		ROSNXT_EAR=Robot_EAR(5) 
		csMsg=ROSNXT_EAR.listen()
		if   ("前进" in csMsg) or ("前" in csMsg)or ("进" in csMsg):
			base_pub.publish("导航前进")
			if not Info_base_syn(ROSNXTBASE,"L导航结束前进",20):
				ROSNXTBASE.ROSNXT_SYS_statuts_talker("L导航故障")
				#break					
			if not Info_base_syn(ROSNXTBASE,"R导航结束前进",10):
				ROSNXTBASE.ROSNXT_SYS_statuts_talker("R导航故障")
				#break
			continue
		if   ("后退" in csMsg) or ("后" in csMsg): 
			base_pub.publish("导航后退")
			if not Info_base_syn(ROSNXTBASE,"L导航结束后退",20):
				ROSNXTBASE.ROSNXT_SYS_statuts_talker("L导航故障")
				#break
			if not Info_base_syn(ROSNXTBASE,"R导航结束后退",10):
				ROSNXTBASE.ROSNXT_SYS_statuts_talker("R导航故障")
				#break
									
			continue
		if   ("左" in csMsg) or ("左转" in csMsg):
			base_pub.publish("导航左转") 
			if not Info_base_syn(ROSNXTBASE,"L导航结束左转",20):
				ROSNXTBASE.ROSNXT_SYS_statuts_talker("L导航故障")
				#break					
			if not Info_base_syn(ROSNXTBASE,"R导航结束左转",10):
				ROSNXTBASE.ROSNXT_SYS_statuts_talker("R导航故障")
				#break					
					
			continue
		if   ("右" in csMsg) or ("右转" in csMsg): 
			base_pub.publish("导航右转")		
			if not Info_base_syn(ROSNXTBASE,"L导航结束右转",20):
				ROSNXTBASE.ROSNXT_SYS_statuts_talker("L导航故障")
				#break					
			if not Info_base_syn(ROSNXTBASE,"R导航结束右转",10):
				ROSNXTBASE.ROSNXT_SYS_statuts_talker("R导航故障")
				#break
			continue
		if   ("停止" in csMsg) or ("停" in csMsg): 
			base_pub.publish("导航停止")
			if not Info_base_syn(ROSNXTBASE,"L导航结束停止",20):
				ROSNXTBASE.ROSNXT_SYS_statuts_talker("L导航故障")
				#break					
			if not Info_base_syn(ROSNXTBASE,"R导航结束停止",20):
				ROSNXTBASE.ROSNXT_SYS_statuts_talker("R导航故障")
				#break
			continue
		if   ("退出" in csMsg) or ("出" in csMsg): 
			base_pub.publish("导航退出")			
			break	

def ROSNXT_EYE_ACTION(ROSNXTEYE):
    global mouse_pub,eye_pub,ROSNXT_EAR
    #Robert Lee
    #j=0    
    #csMsgListName0=['看照片','准备拍照','看照片','退出']				
    #Robert Lee
    while(1):
	mouse_pub.publish("MOUSE_摄像提示")#1.准备拍照，2.摄像，3.退出
	if not Info_mouse_syn(ROSNXTEYE,"摄像控制提示结束",60):
		ROSNXTEYE.ROSNXT_SYS_statuts_talker("MOUSE故障")
		#break
	mouse_pub.publish("MOUSE_摄像控制提示结束")
	print "\n=============摄像提示LISTEN================================\n"			
	ROSNXT_EAR=Robot_EAR(10) 
	csMsg=ROSNXT_EAR.listen()	
	#Robert Lee
	#csMsg=csMsgListName0[j%len(csMsgListName0)]
	#j=j+1
	#Robert Lee	
	print "\n===========开始摄像======命令信息：",csMsg,"======\n"
	#==================================================================
	if   ("退出" in csMsg) or ("出" in csMsg) or("退" in csMsg) : 
		break								
	#==================================================================
	if  ("准备拍照" in csMsg) or ("准备" in csMsg)or ("拍照" in csMsg) :
		bShowPic=False
		#Robert Lee
		#i=0
		#csMsgListName1=['开始拍照','开始拍照','开始拍照','退出']				
		#Robert Lee		
		while(1):		
			if bShowPic==False:
				eye_pub.publish("EYE_开始摄像")
				bShowPic=True						
			mouse_pub.publish("MOUSE_拍照提示") #拍照控制命令：1.开始拍照，2.退出
			if not Info_mouse_syn(ROSNXTEYE,"拍照控制提示结束",60):
				ROSNXTEYE.ROSNXT_SYS_statuts_talker("MOUSE故障")
				#break
			mouse_pub.publish("MOUSE_拍照控制提示结束")				
			print "\n=============MOUSE_拍照提示LISTEN================================\n"
			ROSNXT_EAR=Robot_EAR(10) 
			csMsg=ROSNXT_EAR.listen()			
			#Robert Lee
			#csMsg=csMsgListName1[i%len(csMsgListName1)]
			#i=i+1
			#Robert Lee	
			print "\n=============拍照======命令信息：",csMsg,"======\n"
			#==================================================================
			if   ("退出" in csMsg) or ("出" in csMsg) or("退" in csMsg) : 
				if bShowPic==True:
					eye_pub.publish("EYE_拍照退出")
					if not Info_eye_syn(ROSNXTEYE,"退出完成",10):
						eye_pub.publish("EYE_命令等待超时")				
					bShowPic=False
				break					
			if  ("开始拍照" in csMsg) or ("开始" in csMsg)or ("拍照" in csMsg) :
				eye_pub.publish("EYE_准备开始拍照")
				bShowPic=True
				if not Info_eye_syn(ROSNXTEYE,"完成拍照",10):
					eye_pub.publish("EYE_完成拍照命令等待超时")	
				bShowPic=False
				continue
		continue
			#==================================================================	

	if  ("看照片" in csMsg) or ("看" in csMsg)or ("照片" in csMsg): 
		bShowPic=False
		#Robert Lee
		#k=0
		#csMsgListName2=['第一张照片','下一张照片','上一张照片',"退出"]				
		#Robert Lee		
		while(1):
			mouse_pub.publish("MOUSE_看照片提示")
			if not Info_mouse_syn(ROSNXTEYE,"看照片控制提示结束",60):
				ROSNXTEYE.ROSNXT_SYS_statuts_talker("MOUSE故障")
				#break
			mouse_pub.publish("MOUSE_看照片控制提示结束")
			print "\n=============看照片提示LISTEN================================\n"
			ROSNXT_EAR=Robot_EAR(5) 
			csMsg=ROSNXT_EAR.listen()					
			print "\n=============看照片======命令信息：",csMsg,"======\n"
			#==================================================================
			#Robert Lee
			#csMsg=csMsgListName2[k%len(csMsgListName2)]
			#k=k+1
			#Robert Lee	
			if   ("退出" in csMsg) or ("出" in csMsg) or("退" in csMsg) : 
				eye_pub.publish("EYE_照片退出")
				if not Info_eye_syn(ROSNXTEYE,"退出完成",60):
					eye_pub.publish("EYE_命令等待超时")					
				bShowPic=False
				break						
			if   "第" in csMsg: #"第一张照片"
				if bShowPic==True:
					eye_pub.publish("EYE_照片退出")
					#if not Info_eye_syn(ROSNXTEYE,"退出完成",60):
					#	eye_pub.publish("EYE_命令等待超时")					
					bShowPic=False
				eye_pub.publish("EYE_首张照片")
				bShowPic=True
				continue
			if   "上" in csMsg: #"上一张照片"
				if bShowPic==True:
					eye_pub.publish("EYE_照片退出")
					#if not Info_eye_syn(ROSNXTEYE,"退出完成",60):
					#	eye_pub.publish("EYE_命令等待超时")					
					bShowPic=False
				eye_pub.publish("EYE_上一张照片")
				bShowPic=True
				continue

			if   "下" in csMsg: #"下一张照片"
				if bShowPic==True:
					eye_pub.publish("EYE_照片退出")
					#if not Info_eye_syn(ROSNXTEYE,"退出完成",60):
					#	eye_pub.publish("EYE_命令等待超时")					
					bShowPic=False
				eye_pub.publish("EYE_下一张照片")
				bShowPic=True
				continue
		continue				
		#=================================================================			

def ROSNXT_WEATHER_ACTION(ROSNXTWEATHER):
    global mouse_pub,weather_pub,ROSNXT_EAR 
    while(1):
	
	mouse_pub.publish("MOUSE_开始天气提示")
	if not Info_mouse_syn(ROSNXTWEATHER,"天气控制提示结束",60):
		ROSNXTWEATHER.ROSNXT_SYS_statuts_talker("MOUSE故障")
		break
	print "\n=============天气提示LISTEN=============================\n"
	ROSNXT_EAR=Robot_EAR(10) 
	csMsg=ROSNXT_EAR.listen()					
	print "\n=============天气提示======命令信息：",csMsg,"======\n"
	#==================================================================
	if   ("退出" in csMsg) or ("出" in csMsg) or("退" in csMsg) : 
		break								
	#==================================================================
	if   "本地" in csMsg: 
		weather_pub.publish("WEATHER_本地天气")
		if not Info_weather_syn(ROSNXTWEATHER,"等待",60):
			weather_pub.publish("WEATHER_天气故障")
			#break
		weather_pub.publish("WEATHER_等待")					
		continue
	#==================================================================	
	if   "的天气" in csMsg: 
		weather_pub.publish("WEATHER_%s"%csMsg)
		if not Info_weather_syn(ROSNXTWEATHER,"等待",60):
			weather_pub.publish("WEATHER_天气故障")
		weather_pub.publish("WEATHER_等待")
			#break					
		continue
	#==================================================================	
	if   "天气" in csMsg: 
		weather_pub.publish("WEATHER_%s"%csMsg)
		if not Info_weather_syn(ROSNXTWEATHER,"等待",60):
			weather_pub.publish("WEATHER_天气故障")
		weather_pub.publish("WEATHER_等待")
			#break					
		continue
	#=================================================================

    
def ROSNXT_NODE_Action(ROSNXT):
   #rostopic pub /ROSNXT std_msgs/String "小智退出"      
    global base_pub,mouse_pub,eye_pub,weather_pub,ROSNXT_EAR 
    while(1):    
	#ROSNXT_BASE_ACTION(ROSNXT)
	#continue
	if "小智退出" in ROSNXT.RecInfo:		
		break		
#============================ROSNXT_EAR===================================
	if "等待命令" in ROSNXT.RecInfo:
		ROSNXT.RecInfo="等待命令"
		time.sleep(5)
		continue
	if "听命令" in ROSNXT.RecInfo:
		while(1):			
			if "等待命令" in ROSNXT.RecInfo:
				mouse_pub.publish("MOUSE_系统休息提示")
				if not Info_mouse_syn(ROSNXT,"命令系统休息提示结束",20):
					ROSNXT.ROSNXT_SYS_statuts_talker("MOUSE故障")
					break		

				break
			mouse_pub.publish("MOUSE_听命令提示")
			if not Info_mouse_syn(ROSNXT,"命令控制提示结束",120):
				ROSNXT.ROSNXT_SYS_statuts_talker("MOUSE故障")
				break		
			mouse_pub.publish("MOUSE_命令控制提示结束")
			print "\n=============听命令提示LISTEN=============================\n"
			ROSNXT_EAR=Robot_EAR(10) 
			csMsg=ROSNXT_EAR.listen()
			#Robert Lee
			#csMsg="开始摄像"
			#Robert Lee						
			print "\n==============EAR======命令信息：",csMsg,"================\n"
		        #"查询天气"，"开始摄像"，"语音导航"，"退出"
		        if   "退出" in csMsg:
		        	ROSNXT.ROSNXT_EAR_talker("小智退出")
		        	break                	
		        if   "识别失败" in csMsg: 
		        	continue                	
			if   "查询天气" in csMsg: 
				ROSNXT_WEATHER_ACTION(ROSNXT)
				continue									
			if   "开始摄像" in csMsg: 
				ROSNXT_EYE_ACTION(ROSNXT)
				continue	
			if   "语音导航" in csMsg:
				ROSNXT_BASE_ACTION(ROSNXT)
	continue
def test_func():
    global ROSNXT_EAR  
    ROSNXT_EAR=Robot_EAR(LOGININID)   
    for i in range(5):	
	    ROSNXT_EAR=Robot_EAR(5)   
	    ROSNXT_EAR.listen()
	    print "***************************time ",i,"*************************"    
    exit(0)
                          
if __name__ == '__main__':
#	test_func()
#	exit(0)
	#rostopic pub /ROSNXT std_msgs/String "bbbbb"      
 	global ROSNXT_EAR,sys_status_pub, base_pub,mouse_pub,eye_pub,weather_pub
 	sys_status_pub=rospy.Publisher('ROSNXT_SYS_STATUS', String)
 	base_pub=rospy.Publisher('ROSNXT_BASE', String)
 	mouse_pub=rospy.Publisher('ROSNXT_MOUSE', String)
 	eye_pub=rospy.Publisher('ROSNXT_EYE', String)
 	weather_pub=rospy.Publisher('ROSNXT_WEATHER', String)		
 	threads = [] 
 	ROSNXT_EAR=Robot_EAR(LOGININID)
 	for x in xrange(0, 4):
        	threads.append(threading.Thread(target=thread_main, args=(x,)))
    	for t in threads:
        	t.start()
    	for t in threads:
        	t.join()
 	threads = []
	exit(0)
