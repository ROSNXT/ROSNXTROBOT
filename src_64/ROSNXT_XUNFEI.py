#-*- coding: UTF-8 -*-
################################################################################
#
# File:         ROSNXT_CONSTANTS.py
# RCS:          $Header: $
# Description:  Constants for ROSNXT Control
# Author:       Xubin Li
# Created:      11 13 11:23:04 2014 (Shen Zhen University City) lxbin@uletian.cn
# Modified:     
# Language:     Python
# Package:      ROSNXT
# Status:       Experimental (Do Not Distribute)
#
# 
################################################################################
#
# Revisions:
#
################################################################################
#!/usr/bin/env python
PKG = 'ROSNXT_XUNFEI' # this package name
import _XunFei
from ROSNXT_CONSTANTS import *
class ROSNXT_XUNFEI():
    def __init__(self,appId,server_url = XUNFEIURL, work_dir = "."):
        self.appId=appId
        self.server_url=server_url
        self.work_dir=work_dir
    	
    def login(self):
        return _XunFei.login("appid = %s, server_url = %s, work_dir = %s " % (self.appId,self.server_url,self.work_dir))
    
    def logout(self):
        return _XunFei.logout()
            
    def iat(self,wav_file_path,sub="iat",ssm=1,auf="audio/L16",rate="16000",aue="speex-wb",ent="sms16k",rst="plain",rse="utf8"):
        return _XunFei.iat(wav_file_path,"sub=%s,ssm=%s,auf=%s;rate=%s,aue=%s,ent=%s,rst=%s,rse=%s" % (sub,ssm,auf,rate,aue,ent,rst,rse))
      
    def text_to_speech(self,src_text,vcn="xiaoyan",aue = "speex-wb",auf="audio/L16",rate="16000",spd = "5",vol = "5",tte = "utf8"):
        return _XunFei.text_to_speech(src_text ,"vcn=%s,aue=%s,auf=%s;rate=%s,spd=%s,vol=%s,tte=%s" % (vcn,aue,auf,rate,spd,vol,tte))


