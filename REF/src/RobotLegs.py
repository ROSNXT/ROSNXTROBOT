#!/usr/bin/env python

import roslib; roslib.load_manifest("nxt_python")
import rospy
import time
import nxt.locator
from nxt.sensor import *
from nxt.motor import PORT_A, PORT_B, PORT_C
import nxt.motor 

def test_sensors(b):
        rospy.init_node('test_touchsensor')
        while not rospy.is_shutdown():
	  print 'Touch:',
          if Touch(b, PORT_1).get_sample():
		print 'TOUCH1 ERROR: Touch object'
		
          else:
		print 'TOUCH1 INFO: Nothing'
def test_motorsensors(b):
        rospy.init_node('test_motorsensor')
        while not rospy.is_shutdown():
	  print 'Touch:',
          if Touch(b, PORT_1).get_sample():
	        nxt.motor.Motor(sock, PORT_A).run()
          else:
		nxt.motor.Motor(sock, PORT_A).brake()
		print 'no'



def test_ultrasonicsensors(b):
        rospy.init_node('test_sensor')
        while not rospy.is_shutdown():
	  print 'Test range:',
          print Ultrasonic(b, PORT_2).get_sample()

def test_colorsensors(b):
        rospy.init_node('test_sensor')
        cs = Color20(b, PORT_3)
        print "This is the color sensor test make sure that your color sensor is plugged into port 1:"
	print 'RED:'
        cs.set_light_color(Type.COLORRED)
        time.sleep(1.0)
	print 'BLUE:'
        cs.set_light_color(Type.COLORBLUE)
        time.sleep(1.0)
	print 'GREEN:'
        cs.set_light_color(Type.COLORGREEN)
        time.sleep(1.0)
	print 'FULL:'
        cs.set_light_color(Type.COLORFULL)
        time.sleep(1.0)
	print 'OFF:'
        cs.set_light_color(Type.COLORNONE)
        print "INTENSITY READING:"
        start =rospy.Time.now()
        while rospy.Time.now()<start+rospy.Duration(5.0):
          print 'INTENSITY:', cs.get_reflected_light(Type.COLORBLUE)
          time.sleep(0.1)
        print "COLOR READING:"
        start =rospy.Time.now()
        while rospy.Time.now()<start+rospy.Duration(5.0):
          print 'COLOR:', cs.get_color()
          time.sleep(0.1)
	cs.set_light_color(Type.COLORNONE)
	
#ROS2:00:16:53:0F:59:2B
sock = nxt.locator.find_one_brick(None, 'ROS2', False, None, False, None, None)
if sock:
	nxt.motor.Motor(sock, PORT_A).brake()
#	test_sensors(sock)
	test_motorsensors(sock)
	test_colorsensors(sock)
	test_ultrasonicsensors(sock)	
else:
	print 'No NXT bricks found'
