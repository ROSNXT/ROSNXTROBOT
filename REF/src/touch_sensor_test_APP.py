#!/usr/bin/env python

import roslib; roslib.load_manifest("nxt_python")
import rospy
import time
import nxt.locator
from nxt.sensor import *

def test_sensors(b):
        rospy.init_node('test_sensor')
        print "This is the touch sensor test. Make sure that your touch sensor is plugged into port 1:"
        print "TOUCH READING:"
        start =rospy.Time.now()
        while rospy.Time.now()<start+rospy.Duration(5.0):
          #print 'TOUCH:', Touch(b, PORT_1).get_sample()
	  print 'TOUCH:', Color20(b, PORT_3).set_light_color(Type.COLORNONE)
          time.sleep(0.1)


sock = nxt.locator.find_one_brick()
if sock:
	test_sensors(sock)

else:
	print 'No NXT bricks found'
