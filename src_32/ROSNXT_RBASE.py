#-*- coding: UTF-8 -*-
################################################################################
#
# File:         ROSNXT_CONSTANTS.py
# RCS:          $Header: $
# Description:  Constants for ROSNXT Control
# Author:       Xubin Li
# Created:      11 13 11:23:04 2014 (Shen Zhen University City) lxbin@uletian.cn
# Modified:     
# Language:     Python
# Package:      ROSNXT
# Status:       Experimental (Do Not Distribute)
#
# 
################################################################################
#
# Revisions:
#
################################################################################
#!/usr/bin/env python
PKG = 'NXTROS_RBASE' # this package name
import roslib
import rospy
from std_msgs.msg import String    
import threading

import rospy
import nxt.locator
from nxt.sensor import *
import roslib
from nxt.motor import PORT_A, PORT_B, PORT_C
import sys,time
from std_msgs.msg import String
from ROSNXT_CONSTANTS import TIMEFORTHLONG,BASEFORTHPOWER
from ROSNXT_CONSTANTS import TIMETURNLONG,BASETURNPOWER
from ROSNXT_CONSTANTS import MINDISTANCE,MINLOWCOUNT
iDisCount=0
iCoutType=0
eye_talker = rospy.Publisher('ROSNXT_EYE', String)
ear_talker = rospy.Publisher('ROSNXT', String)

def get_touch1sensors(b):
        Value=Touch(b, PORT_1).get_sample()
        Msg='Touch1'+'State:'+str(Value)
	return Msg
def get_ultrasonicsensors(b,csType):

        DistanceValue=Ultrasonic(b, PORT_2).get_sample()
        global iDisCount,iCoutType
                       	
        if (DistanceValue<=MINDISTANCE):
        	iDisCount=iDisCount+1
        else:        		
        	if  (iDisCount>=MINLOWCOUNT)==1:
			iDisCount=0
        		iCoutType=iCoutType+1
        		if iCoutType%2==1:
        			if csType=="ROS1" :  
					eye_talker.publish("开始跟踪")
				else:
					ear_talker.publish("听命令")
			else:	
        			if csType=="ROS1" :  					
					eye_talker.publish("EYE_跟踪退出")					
				else:
					ear_talker.publish("等待命令")

                
        noseMsg='ROS2_Nose1_Distance:'+str(DistanceValue)
	return noseMsg
	
	
def get_touch2sensors(b):
        Value=Touch(b, PORT_3).get_sample()
        Msg='Touch2'+'State:'+str(Value)
	return Msg
def set_colorsensors(b,csROSNXTState):
        cs = Color20(b, PORT_4)
        #print "This is the color sensor test make sure that your color sensor is plugged into port 4:"
	if "左导航" in csROSNXTState:
		print 'RED:'
	        cs.set_light_color(Type.COLORRED)
	if "后左导航" in csROSNXTState:        
		print 'BLUE:'
		cs.set_light_color(Type.COLORBLUE)
	if "右导航" in csROSNXTState:              
		print 'GREEN:'
		cs.set_light_color(Type.COLORGREEN)
	if "后右导航" in csROSNXTState: 
		print 'FULL:'
		cs.set_light_color(Type.COLORFULL)		
	if "正常" in csROSNXTState:        
		print 'OFF:'
	        cs.set_light_color(Type.COLORNONE)
	return csROSNXTState
def get_colorsensors(b):
	cs = Color20(b, PORT_4)
	return cs.get_color()
	        
def set_motorbrake(b):
	nxt.motor.Motor(b, PORT_A).brake()
	nxt.motor.Motor(b, PORT_B).brake()

def set_lmotorsensors(b,iPower,csType):
	if csType=="ROS1":
		nxt.motor.Motor(b, PORT_A).run(iPower,False)
	if csType=="ROS2":
		nxt.motor.Motor(b, PORT_A).run(-1*iPower,False)
		
def set_rmotorsensors(b,iPower,csType):
	if csType=="ROS1":
		nxt.motor.Motor(b, PORT_B).run(iPower,False)
	if csType=="ROS2":
		nxt.motor.Motor(b, PORT_B).run(-1*iPower,False)

def thread_main(a):
    global ROSNXT_RBASE
    threadname = threading.currentThread().getName()
    if a==0:	
    	print "====================RBASE Start SYSTEM Listen Threading ================="
        ROSNXT_SYS_listener(ROSNXT_RBASE)
    if a==1:
    	print "====================RBASE Wait NODE Listen Threading ======================"
        ROSNXT_NODE_listener(ROSNXT_RBASE)

    if a==2:
    	print "====================RBASE Start Action Threading ========================="
        ROSNXT_SYS_Action(ROSNXT_RBASE)
    if a==3:
    	print "====================RBASE Start Action Threading ========================="
        ROSNXT_NODE_Action(ROSNXT_RBASE)
    	
    print threadname, a
def ROSNXT_SYS_listener(ROSNXT):
    ROSNXT.ROSNXT_sys_listener()
    return 

def ROSNXT_NODE_listener(ROSNXT):
    ROSNXT.ROSNXT_node_listener()
    return 


def ROSNXT_SYS_Action(ROSNXT):
   #rostopic pub /ROSNXT std_msgs/String "小智退出"
    while(1):
	
	if "小智退出" in ROSNXT.SYSRecInfo:
		ROSNXT.ROSNXT_BASE_talker("导航系统退出")
		break
#==========================================================================================				
	if "RBASE故障" in ROSNXT.SYSRecInfo:
		ROSNXT.ROSNXT_SYS_statuts_talker("RBASE故障")
		continue
	if "RBASE空闲" in ROSNXT.SYSRecInfo:
		ROSNXT.ROSNXT_SYS_statuts_talker("RBASE空闲")
		continue
	
    return 
    
def ROSNXT_NODE_Action(ROSNXT):    
   #rostopic pub /ROSNXT std_msgs/String "小智退出"
    while(1):
	ROSNXT.test_run()
	ROSNXT.leg_stop()
	if "导航系统退出" in ROSNXT.BaseRecInfo:	
		break
	if "导航退出" in ROSNXT.BaseRecInfo:	
		continue	
#============================ROSNXT_BASE===================================			
#============================ROSNXT_BASE===================================			
	if   "导航前进" in ROSNXT.BaseRecInfo: 
		ROSNXT.leg_goforth_run(TIMEFORTHLONG,BASEFORTHPOWER)		
		if ROSNXT.csType=="ROS2":
			time.sleep(1)
		ROSNXT.ROSNXT_BASE_talker("R导航结束前进")
		continue
	if   "导航后退" in ROSNXT.BaseRecInfo: 
		ROSNXT.leg_goforth_run(TIMEFORTHLONG,BASEFORTHPOWER*-1)			
		if ROSNXT.csType=="ROS2":
			time.sleep(1)
		
		ROSNXT.ROSNXT_BASE_talker("R导航结束后退")
		continue
	if   "导航左转" in ROSNXT.BaseRecInfo:
		ROSNXT.leg_turn_run(TIMETURNLONG,BASETURNPOWER,BASETURNPOWER*-1)			
		if ROSNXT.csType=="ROS2":
			time.sleep(1)

		ROSNXT.ROSNXT_BASE_talker("R导航结束左转")				
		continue
	if   "导航右转" in ROSNXT.BaseRecInfo:
		ROSNXT.leg_turn_run(TIMETURNLONG,BASETURNPOWER*-1,BASETURNPOWER)			
		if ROSNXT.csType=="ROS2":
			time.sleep(1)
		ROSNXT.ROSNXT_BASE_talker("R导航结束右转")
		continue
	if   "导航停止" in ROSNXT.BaseRecInfo:
		ROSNXT.leg_stop()
		if self.csType=="ROS2":
			time.sleep(1)
		ROSNXT.ROSNXT_BASE_talker("R导航结束停止")		
		continue	
    return 

class Robot_BASE():
    def __init__(self):
	rospy.init_node('Robot_BASE_ROSNXT', anonymous=True)
	self.talker = rospy.Publisher('ROSNXT_BASE', String)

	
	self.SYSRecInfo=""
	self.BaseRecInfo=""
    
    	self.sock=None
	self.csROSNXTState="正常"
	self.csLastROSNXTState="正常"
	self.bTouch1Status=False
	self.bTouch2Status=False
	self.LEGsync=True

	try:
		self.sock = nxt.locator.find_one_brick(None, 'ROS2', False, None, False,None, None)	    	
		self.csType="ROS2"
	except:
		print 'No ROS2 NXT bricks init No found'		
    	set_colorsensors(self.sock,self.csROSNXTState)
    	
	return

	return 
    def arm1_run(self):
	try:
		self.csMsg=get_touch1sensors(self.sock)
		print self.csMsg
		if "True" in self.csMsg:
			self.bTouch1Status=True
		else:
		 	self.bTouch1Status=False
	except:
		print 'arm1_run ROS2 NXT bricks No found'		
    def arm2_run(self):
	try:
		self.csMsg=get_touch2sensors(self.sock)
		print self.csMsg
		if "True" in self.csMsg:
			self.bTouch2Status=True
		else:
		 	self.bTouch2Status=False

	except:
		print 'arm2_run ROS2 NXT bricks No found'		

    def nose_run(self):
	try:
		self.csMsg=get_ultrasonicsensors(self.sock,self.csType)
		print self.csMsg
	except:
		print 'nose_run ROS2 NXT bricks No found'

    def leg_stop(self):
    	set_motorbrake(self.sock)
					
    def leg_goforth_run(self,secs,power=10):
    	set_motorbrake(self.sock)
    	
    	if self.LEGsync==True:
		try:	
			print "=================AAA============================"
			set_lmotorsensors(self.sock,power,self.csType)
			print "=================BBB============================"
			set_rmotorsensors(self.sock,power,self.csType)
			print "=================CCC============================"			
			time.sleep(secs)
			print "=================DDD============================"
			set_motorbrake(self.sock)				
			print "=================EEE============================"					
		except:
			print 'leg_goforth run ROS2 NXT bricks No found'		


    def leg_turn_run(self,secs,lpower=10,rpower=-10):
    	set_motorbrake(self.sock)    
    	if self.LEGsync==True:
		try:
			set_lmotorsensors(self.sock,lpower,self.csType)
			set_rmotorsensors(self.sock,rpower,self.csType)
			time.sleep(secs)
   		 	set_motorbrake(self.sock)
		except:
			print 'nose_run ROS2 NXT bricks No found'
   #def set_rmotorsensors(b,iPower,csType):		
    		
    def getState(self):
    	if self.bTouch1Status==True and self.csType=="ROS1":
    		return "左导航"
    	if self.bTouch2Status==True and self.csType=="ROS1":
    		return "后左导航"
    	if self.bTouch1Status==True and self.csType=="ROS2":
    		return "右导航"
    	if self.bTouch2Status==True and self.csType=="ROS2":
    		return "后右导航"
    	return "正常"

    def state_run(self):
	try:
		self.csROSNXTState=self.getState()
		print self.csROSNXTState
		if self.csLastROSNXTState!=self.csROSNXTState:
			print "Change State"
			self.csLastROSNXTState=set_colorsensors(self.sock,self.csROSNXTState)
	except:
		print 'state_run ROS2 NXT bricks No found'		
    def test_run(self):
    	self.arm1_run()		
    	self.nose_run()    	
    	self.arm2_run()    	
    	self.state_run()
    def test_leg(self,i=0):
    	self.arm1_run()		
    	#self.nose_run()    	
    	self.arm2_run()    	
    	self.state_run()
    	self.leg_goforth_run(5,-50)
	if self.csROSNXTState=="左导航":
		self.leg_goforth_run(2,40)
	if self.csROSNXTState=="右导航":
		self.leg_goforth_run(2,-40)
	if self.csROSNXTState=="后左导航":
		self.leg_turn_run(2,10,-10)
	if self.csROSNXTState=="后右导航":
		self.leg_turn_run(2,-10,10)
	
    def __del__(self):
    	self.sock=None    
	return

	
    def callback(self,data):
    	#rospy.loginfo("\nROSNXT_RBASE heard: %s",data.data)
	self.BaseRecInfo=data.data
	print self.BaseRecInfo
	
    def system_callback(self,data):
	#rospy.loginfo("\nROSNXT SYSTEM heard: %s",data.data)
	self.SYSRecInfo=data.data
	print self.SYSRecInfo
	        
    def ROSNXT_node_listener(self):
	self.listener=rospy.Subscriber('ROSNXT_BASE', String, self.callback)
	# in ROS, nodes are unique named. If two nodes with the same
	# node are launched, the previous one is kicked off. The 
	# anonymous=True flag means that rospy will choose a unique
	# name for our 'talker' node so that multiple talkers can
	# run simultaenously.

	# spin() simply keeps python from exiting until this node is stopped
	rospy.spin()
		
    def ROSNXT_sys_listener(self):
	self.listener=rospy.Subscriber('ROSNXT_SYS_STATUS', String, self.system_callback)
	# in ROS, nodes are unique named. If two nodes with the same
	# node are launched, the previous one is kicked off. The 
	# anonymous=True flag means that rospy will choose a unique
	# name for our 'talker' node so that multiple talkers can
	# run simultaenously.
	# spin() simply keeps python from exiting until this node is stopped
	rospy.spin()

    def ROSNXT_BASE_talker(self,data):
	#self.talker = rospy.Publisher('ROSNXT', String)
	
  	self.str = "RBASE_BASE_%s"%data    	
   	self.talker.publish(self.str)

	
    def ROSNXT_SYS_statuts_talker(self,data):
	global sys_status_pub	
   	self.str = "SYSTEM_%s"%data    	
   	sys_status_pub.publish(self.str)


def test():
	print "=======***************************====ROS2 WAIT====*************************======="
	time.sleep(10)	
	a=Robot_BASE()
	for i in range(20):	
    		#a.test_run(i)
    		a.test_leg()
        	print "=======***************************====time ",i,"============*************************======="    	
	exit(0)
	return 
if __name__ == '__main__':
 	global ROSNXT_RBASE,sys_status_pub
 	sys_status_pub=rospy.Publisher('ROSNXT_SYS_STATUS', String) 	
 	try:
 		ROSNXT_RBASE=Robot_BASE()
 	except:
 		sys_status_pub.publish("RBASE故障")
 		exit(0) 	
 	threads = [] 
 	for x in xrange(0, 4):
        	threads.append(threading.Thread(target=thread_main, args=(x,)))
    	for t in threads:
        	t.start()
    	for t in threads:
        	t.join()
 	threads = []
	exit(0)	
