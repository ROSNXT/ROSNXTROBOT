#-*- coding: UTF-8 -*-
################################################################################
#
# File:         ROSNXT_CONSTANTS.py
# RCS:          $Header: $
# Description:  Constants for ROSNXT Control
# Author:       Xubin Li
# Created:      11 13 11:23:04 2014 (Shen Zhen University City) lxbin@uletian.cn
# Modified:     
# Language:     Python
# Package:      ROSNXT
# Status:       Experimental (Do Not Distribute)
#
# 
################################################################################
#
# Revisions:
#
################################################################################
import roslib
import rospy
from std_msgs.msg import String    
import threading
import time
import urllib2,string  
from urllib2 import Request, urlopen  
from ROSNXT_CONSTANTS import CITYCODEURL,WEATHERCITYURL,CITYCODE,CITYNUM

class SERVICE_Weather():
	def __init__(self,info):		  
		  rospy.init_node('Robot_WEATHER_ROSNXT', anonymous=True)
		  self.talker = rospy.Publisher('ROSNXT_WEATHER', String)
		  self.WeatherRecInfo=""
		  self.SYSRecInfo=""
	
		  self.url1=CITYCODEURL
		  self.info=info
		  self.city_code=None		  
		  			      		       
	def city_Code(self,city_name):#获取任意城市的天气信息 

		  self.city_file=open(CITYCODE,"r")#city.txt中存放着各城市的编码，这个编码详见附录。 
		  for i in range (0,CITYNUM): 
		    self.city_info=self.city_file.readline() 
		    if city_name in self.city_info: 
		      self.city_code=self.city_info[:9] 
		      break
		  self.city_file.close
		  if i >= CITYNUM:
		  	return -1 
		  #print  self.city_code	
		  return self.city_code		  
	def getWeather(self):#获取当地的天气信息
				  
		  if '的天气'in self.info:  
		    	self.info=self.info[:self.info.index('的天气')]		    	  
			if self.city_Code(self.info)  ==-1:
			      	#print '无该城市'  
				return '无该城市' 
		  elif '天气' in self.info: 		  	
		   	  if '天气' == self.info:		   	  		 		  
				  try: 
				    	self.page1=urllib2.urlopen(self.url1)  
				  except IOError, e:  
				    	if hasattr(e, 'reason'):  
				      		print '请检查您的网络连接',e  
				      		self.page1=None  
				    	elif hasattr(e, 'code'):  
				      		print "请检查您的网络连接",e  
				      		self.page1=None
				  if self.page1==None:
				  	  return "本地城市连接失败"
				  else:  
					  self.data1=self.page1.read()
					  self.city_code=self.data1[self.data1.index('d=')+2:self.data1.index('d=')+11]
			  else:	  	
				self.info=self.info[:self.info.index('天气')]
				if self.city_Code(self.info) ==-1:
				      	print '无该城市'  
					return '无该城市' 
		  return self.weathermsg(self.city_code)
		  			  
	def weathermsg(self,city_code):
		  self.url2=''.join([WEATHERCITYURL,city_code,'.html'])  
		  #print self.url2
		  self.bPage=False
		  for i in range(10):
			  try:
			  	self.page2=urllib2.urlopen(self.url2)
			  	self.bPage=True
			  	break
			  except:
			  	print "天气网站连接失败"
			  	time.sleep(5)
		  if self.bPage==False:
		  	return "本地天气网站连接失败"
		  else:  			    
		    self.data2=self.page2.read()
		    self.counter=0  
		    for i in range (0,len(self.data2)):  
		      if self.data2[i]== '"' : 
			self.counter+=1 
			if self.counter==5: 
			  self.city1=i 
			elif self.counter==6: 
			  self.city2=i 
			elif self.counter==13: 
			  self.l_temp1=i 
			elif self.counter==14: 
			  self.l_temp2=i 
			elif self.counter==17: 
			  self.h_temp1=i 
			elif self.counter==18: 
			  self.h_temp2=i 
			elif self.counter==21: 
			  self.weather1=i 
			elif self.counter==22: 
			  self.weather2=i				   
		    self.city=self.data2[self.city1+1:self.city2] 
		    self.weather=self.data2[self.weather1+1:self.weather2] 
		    self.h_temp=self.data2[self.h_temp1+1:self.h_temp2] 
		    self.l_temp=self.data2[self.l_temp1+1:self.l_temp2] 
		    self.result='  '.join([self.city,self.weather,self.l_temp+'~'+self.h_temp]) 
		    print self.result 	
		    return self.result
		    
	def callback(self,data):
    		#rospy.loginfo("\nROSNXT_WEATHER heard: %s",data.data)
		self.WeatherRecInfo=data.data
		#print self.WeatherRecInfo
	
	def system_callback(self,data):
		#rospy.loginfo("\nROSNXT SYSTEM heard: %s",data.data)
		self.SYSRecInfo=data.data
		#print self.SYSRecInfo
        
	def ROSNXT_node_listener(self):
		self.listener=rospy.Subscriber('ROSNXT_WEATHER', String, self.callback)
		# in ROS, nodes are unique named. If two nodes with the same
		# node are launched, the previous one is kicked off. The 
		# anonymous=True flag means that rospy will choose a unique
		# name for our 'talker' node so that multiple talkers can
		# run simultaenously.

		# spin() simply keeps python from exiting until this node is stopped
		rospy.spin()
		
	def ROSNXT_sys_listener(self):
		self.listener=rospy.Subscriber('ROSNXT_SYS_STATUS', String, self.system_callback)
		# in ROS, nodes are unique named. If two nodes with the same
		# node are launched, the previous one is kicked off. The 
		# anonymous=True flag means that rospy will choose a unique
		# name for our 'talker' node so that multiple talkers can
		# run simultaenously.

		# spin() simply keeps python from exiting until this node is stopped
		rospy.spin()
    
	def ROSNXT_WEATHER_talker(self,data):

	    	self.str = "WEATHER_%s"%data    	
    		self.talker.publish(self.str)
	
	def ROSNXT_SYS_statuts_talker(self,data):
		global sys_status_pub	
	    	self.str = "SYSTEM_%s"%data    	
    		sys_status_pub.publish(self.str)
        
def thread_main(a):
    global ROSNXT_WEATHER
    threadname = threading.currentThread().getName()
    if a==0:	
    	print "====================Start SYSTEM Listen Threading ================="
        ROSNXT_SYS_listener(ROSNXT_WEATHER)
    if a==1:
    	print "====================Wait NODE Listen Threading ======================"
        ROSNXT_NODE_listener(ROSNXT_WEATHER)
    if a==2:
    	print "====================Start Action Threading ========================="
        ROSNXT_SYS_Action(ROSNXT_WEATHER)
    if a==3:
    	print "====================Start Action Threading ========================="
        ROSNXT_NODE_Action(ROSNXT_WEATHER)
    	
    print threadname, a
def ROSNXT_SYS_listener(ROSNXT):
    ROSNXT.ROSNXT_sys_listener()
    return 

def ROSNXT_NODE_listener(ROSNXT):
    ROSNXT.ROSNXT_node_listener()
    return 
def ROSNXT_SYS_Action(ROSNXT):
    while(1):
	if "退出" in ROSNXT.SYSRecInfo:	
		ROSNXT.ROSNXT_WEATHER_talker("退出")
		break
	if "WEATHER故障" in ROSNXT.SYSRecInfo:
		ROSNXT.ROSNXT_SYS_statuts_talker("WEATHER故障")
		ROSNXT.SYSRecInfo="WEATHER故障"
		continue
    return 
    
def ROSNXT_NODE_Action(ROSNXT):
   #rostopic pub /ROSNXT std_msgs/String "小智退出"      
    global ROSNXT_WEATHER,sys_status_pub,mouse_pub
    while(1):	
	if "小智退出" in ROSNXT.WeatherRecInfo:	
		break		
#===========================ROSNXT_EYE====================================
	if "天气退出" in ROSNXT.WeatherRecInfo:			
		ROSNXT.ROSNXT_WEATHER_talker("天气查询退出")
		continue
	if "天气的结束" in ROSNXT.WeatherRecInfo or "天气本地结束" in ROSNXT.WeatherRecInfo:
		ROSNXT.ROSNXT_WEATHER_talker("等待")
		continue
	if "本地天气" in ROSNXT.WeatherRecInfo:
		try:
			ROSNXT_WEATHER=SERVICE_Weather("天气")
			csInfo=ROSNXT_WEATHER.getWeather()			
			mouse_pub.publish("播报天气%s"%csInfo)
			if not Info_mouse_syn("天气播报结束",60):				
				ROSNXT.ROSNXT_SYS_statuts_talker("MOUSE故障")
				break
			ROSNXT.ROSNXT_WEATHER_talker("天气本地结束")
			continue
		except:
			mouse_pub.publish("%s 天气查询失败"%ROSNXT.WeatherRecInfo)
			ROSNXT.ROSNXT_WEATHER_talker("天气本地结束")			
			continue
	if "的天气" in ROSNXT.WeatherRecInfo or "天气" in ROSNXT.WeatherRecInfo:
		print "查询开始的天气信息：",ROSNXT.WeatherRecInfo[ROSNXT.WeatherRecInfo.index('_')+1:]
		if "天气故障" in ROSNXT.WeatherRecInfo:	
			mouse_pub.publish("%s 天气查询失败"%ROSNXT.WeatherRecInfo)
			ROSNXT.ROSNXT_WEATHER_talker("天气的结束")
			continue
		try:			
			print "查询的天气：",ROSNXT.WeatherRecInfo[ROSNXT.WeatherRecInfo.index('_')+1:]
			
			ROSNXT_WEATHER=SERVICE_Weather(ROSNXT.WeatherRecInfo[ROSNXT.WeatherRecInfo.index('_')+1:])
			csInfo=ROSNXT_WEATHER.getWeather()
			mouse_pub.publish("播报天气%s"%csInfo)
			if not Info_mouse_syn("天气播报结束",60):
				ROSNXT.ROSNXT_SYS_statuts_talker("MOUSE故障")
				break
			ROSNXT.ROSNXT_WEATHER_talker("天气的结束")
		except:

			mouse_pub.publish("%s 天气查询失败"%ROSNXT.WeatherRecInfo)
			ROSNXT.ROSNXT_WEATHER_talker("天气的结束")
			continue
			
    return 
def test():
	for i in range(20): 
		a=SERVICE_Weather("天气")
		a.getWeather()
		a=SERVICE_Weather("南昌的天气")
		a.getWeather()
    		print "=======***************************====time ",i,"============*************************======="		
def Info_mouse_syn(ExpectInfo,Timout):
	global  mouse_Info	
        timeout_t = time.time() + Timout #10 seconds
	while  time.time() < timeout_t:
		mouse_listen=rospy.Subscriber('ROSNXT_MOUSE', String, mouse_callback)
		print "MOUSE:",mouse_Info,"ExpectInfo:",ExpectInfo
		if ExpectInfo in mouse_Info:
			return True            
		time.sleep(1)		
	if time.time() > timeout_t:
		return False    
def mouse_callback(data):
	global  mouse_Info
	mouse_Info = str(data.data) 
	

if __name__=='__main__':
#	test()
#	exit(0)
 	global ROSNXT_WEATHER,sys_status_pub,mouse_pub,mouse_listen,mouse_Info
 	mouse_pub=rospy.Publisher('ROSNXT_MOUSE', String)
 	ROSNXT_WEATHER=SERVICE_Weather("天气")
 	#ROSNXT_WEATHER.getWeather()
 	sys_status_pub=rospy.Publisher('ROSNXT_SYS_STATUS', String) 	
 	threads = [] 
 	for x in xrange(0, 4):
        	threads.append(threading.Thread(target=thread_main, args=(x,)))
    	for t in threads:
        	t.start()
    	for t in threads:
        	t.join()
 	threads = []
	exit(0)	    
						
