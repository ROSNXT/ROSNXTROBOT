//################################################################################
//#
//# File:         ROSNXT_CONSTANTS.py
//# RCS:          $Header: $
//# Description:  Constants for ROSNXT Control
//# Author:       Xubin Li
//# Created:      11 13 11:23:04 2014 (Shen Zhen University City) lxbin@uletian.cn
//# Modified:     
//# Language:     Python
//# Package:      ROSNXT
//# Status:       Experimental (Do Not Distribute)
//#
//# 
//################################################################################
//#
//# Revisions:
//#
//################################################################################
#include "/usr/include/python2.7/Python.h" //包含python的头文件
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include "./include/qtts.h"
#include "./include/qisr.h"
#include "./include/msp_cmn.h"
#include "./include/msp_errors.h"

// 1 c/cpp中的函数
typedef int SR_DWORD;
typedef short int SR_WORD ;

//音频头部格式
struct wave_pcm_hdr
{
	char            riff[4];                        // = "RIFF"
	SR_DWORD        size_8;                         // = FileSize - 8
	char            wave[4];                        // = "WAVE"
	char            fmt[4];                         // = "fmt "
	SR_DWORD        dwFmtSize;                      // = 下一个结构体的大小 : 16

	SR_WORD         format_tag;              // = PCM : 1
	SR_WORD         channels;                       // = 通道数 : 1
	SR_DWORD        samples_per_sec;        // = 采样率 : 8000 | 6000 | 11025 | 16000
	SR_DWORD        avg_bytes_per_sec;      // = 每秒字节数 : dwSamplesPerSec * wBitsPerSample / 8
	SR_WORD         block_align;            // = 每采样点字节数 : wBitsPerSample / 8
	SR_WORD         bits_per_sample;         // = 量化比特数: 8 | 16

	char            data[4];                        // = "data";
	SR_DWORD        data_size;                // = 纯数据长度 : FileSize - 44 
} ;

//默认音频头部数据
struct wave_pcm_hdr default_pcmwavhdr = 
{
	{ 'R', 'I', 'F', 'F' },
	0,
	{'W', 'A', 'V', 'E'},
	{'f', 'm', 't', ' '},
	16,
	1,
	1,
	16000,
	32000,
	2,
	16,
	{'d', 'a', 't', 'a'},
	0  
};

int xf_run_text_to_speech(const char* src_text  ,const char* params)
{
	struct wave_pcm_hdr pcmwavhdr = default_pcmwavhdr;
	const char* des_path="text_to_speech.wav";
	const char* sess_id = NULL;
	int ret = 0;
	unsigned int text_len = 0;
	char* audio_data;
	unsigned int audio_len = 0;
	int synth_status = 1;
	FILE* fp = NULL;

	printf("\nbegin to synth...\n");
	if (NULL == src_text || NULL == des_path)
	{
		printf("params is null!\n");
		return -1;
	}
	text_len = (unsigned int)strlen(src_text);
	fp = fopen(des_path,"wb");
	if (NULL == fp)
	{
		printf("open file %s error\n",des_path);
		return -1;
	}
	sess_id = QTTSSessionBegin(params, &ret);
	if ( ret != MSP_SUCCESS )
	{
		printf("QTTSSessionBegin: qtts begin session failed Error code %d.\n",ret);
		return ret;
	}

	ret = QTTSTextPut(sess_id, src_text, text_len, NULL );
	if ( ret != MSP_SUCCESS )
	{
		printf("QTTSTextPut: qtts put text failed Error code %d.\n",ret);
		QTTSSessionEnd(sess_id, "TextPutError");
		return ret;
	}
	fwrite(&pcmwavhdr, sizeof(pcmwavhdr) ,1, fp);
	while (1) 
	{
		const void *data = QTTSAudioGet(sess_id, &audio_len, &synth_status, &ret);
		if (NULL != data)
		{
		   fwrite(data, audio_len, 1, fp);
		   pcmwavhdr.data_size += audio_len;//修正pcm数据的大小
		}
              printf("\nget audio...\n");
              usleep(150000);//建议可以sleep下，因为只有云端有音频合成数据，audioget都能获取到音频。
		if (synth_status == 2 || ret != 0) 
		break;
	}

	//修正pcm文件头数据的大小
	pcmwavhdr.size_8 += pcmwavhdr.data_size + 36;

	//将修正过的数据写回文件头部
	fseek(fp, 4, 0);
	fwrite(&pcmwavhdr.size_8,sizeof(pcmwavhdr.size_8), 1, fp);
	fseek(fp, 40, 0);
	fwrite(&pcmwavhdr.data_size,sizeof(pcmwavhdr.data_size), 1, fp);
	fclose(fp);

	ret = QTTSSessionEnd(sess_id, NULL);
	if ( ret != MSP_SUCCESS )
	{
	printf("QTTSSessionEnd: qtts end failed Error code %d.\n",ret);
	}
       printf("\nTTS end...\n");
	return ret;
}


int xf_login(const char* login_configs){
    //const char* login_configs = "appid = 1234b6dd, server_url = http://dev.voicecloud.cn/index.htm, work_dir =   .  ";
	int ret = 0; 

	//用户登录
	ret = MSPLogin(NULL, NULL, login_configs);
	if ( ret != MSP_SUCCESS )
	{
		printf("MSPLogin failed , Error code %d.\n",ret);
	}
	return ret;
}

void xf_run_iat(const char* src_wav_filename ,  const char* param, char * rec_result)
{
//const char* param1 = "sub=iat,ssm=1,auf=audio/L16;rate=16000,aue=speex,ent=sms16k,rst=plain,rse=gb2312";//直接转写，默认编码为gb2312，可以通过rse参数指定为utf8或unicode
	
	const char *sessionID;
	FILE *f_pcm = NULL;
	char *pPCM = NULL;
	int lastAudio = 0 ;
	int audStat = 2 ;
	int epStatus = 0;
	int recStatus = 0 ;
	long pcmCount = 0;
	long pcmSize = 0;
	int errCode = 10 ;

	sessionID = QISRSessionBegin(NULL, param, &errCode);
	f_pcm = fopen(src_wav_filename, "rb");
	if (NULL != f_pcm) {
		fseek(f_pcm, 0, SEEK_END);
		pcmSize = ftell(f_pcm);
		fseek(f_pcm, 0, SEEK_SET);
		pPCM = (char *)malloc(pcmSize);
		fread((void *)pPCM, pcmSize, 1, f_pcm);
		fclose(f_pcm);
		f_pcm = NULL;
	}
	while (1) {
		unsigned int len = 6400;
		int ret;

		if (pcmSize < 12800) {
			len = pcmSize;
			lastAudio = 1;
		}
		audStat = 2;
		if (pcmCount == 0)
			audStat = 1;
		if (0) {
			if (audStat == 1)
				audStat = 5;
			else
				audStat = 4;
		}
		if (len<=0)
		{
			break;
		}
// 		printf("csid=%s,count=%d,aus=%d,",sessionID,pcmCount/len,audStat);
		ret = QISRAudioWrite(sessionID, (const void *)&pPCM[pcmCount], len, audStat, &epStatus, &recStatus);
// 		printf("eps=%d,rss=%d,ret=%d\n",epStatus,recStatus,errCode);
		if (ret != 0)
			break;
		pcmCount += (long)len;
		pcmSize -= (long)len;
		if (recStatus == 0) {
			const char *rslt = QISRGetResult(sessionID, &recStatus, 0, &errCode);
			if (NULL != rslt){
				printf("%s\n", rslt);
				strcat(rec_result,rslt);
			}
		}
		if (epStatus == MSP_EP_AFTER_SPEECH)
			break;
		usleep(150000);
	}
	QISRAudioWrite(sessionID, (const void *)NULL, 0, 4, &epStatus, &recStatus);
	free(pPCM);
	pPCM = NULL;
	while (recStatus != 5 && errCode == 0) {
		const char *rslt = QISRGetResult(sessionID, &recStatus, 0, &errCode);
		if (NULL != rslt)
		{
			strcat(rec_result,rslt);
		}
		usleep(150000);
	}
	QISRSessionEnd(sessionID, NULL);
// 	printf("=============================================================\n");
 	printf("The result is: %s\n",rec_result);
// 	printf("=============================================================\n");
 
}

// 2 python 包装

static PyObject * wrap_xf_login(PyObject *self, PyObject *args) {
  const char * configs; 
  int n;
  if (!PyArg_ParseTuple(args, "s", &configs))//这句是把python的变量args转换成c的变量configs
    return NULL;
  n = xf_login(configs );//调用c的函数
  return Py_BuildValue("i", n);//把c的返回值n转换成python的对象
}

static PyObject * wrap_xf_logout(PyObject *self, PyObject *args) {
    int n = MSPLogout( );//调用c的函数 
    return Py_BuildValue("i", n);
}

static PyObject * wrap_xf_run_iat(PyObject *self, PyObject *args) {
  const char * src_wav_filename;
  const char * param; 
  if (!PyArg_ParseTuple(args, "ss", &src_wav_filename,&param))//这句是把python的变量args转换成c的变量configs
    return NULL;
    char rec_result[1024*4] = {0};
  xf_run_iat(src_wav_filename,param,rec_result);//调用c的函数
  return Py_BuildValue("s", rec_result);//把c的返回值n转换成python的对象
} 
static PyObject * wrap_xf_run_text_to_speech(PyObject *self, PyObject *args) {
  const char * src_text;
//  const char * des_path;
  const char * param;  
  int n;
  if (!PyArg_ParseTuple(args, "ss", &src_text,&param))//这句是把python的变量args转换成c的变量configs
    return NULL;
  n=xf_run_text_to_speech(src_text,param);//调用c的函数
  return Py_BuildValue("i", n);//把c的返回值n转换成python的对象
} 



// 3 方法列表
static PyMethodDef MyCppMethods[] = {
    //init是python中注册的函数名，wrap_xf_login 
    { "login", wrap_xf_login, METH_VARARGS, "login,link to libmsc.MSPLogin" },
    { "logout", wrap_xf_logout, METH_VARARGS, "logout,link to libmsc.MSPLogout ." },
    { "iat", wrap_xf_run_iat, METH_VARARGS, "run_iat." },
    { "text_to_speech", wrap_xf_run_text_to_speech, METH_VARARGS, "run_text_to_speech." },    
    { NULL, NULL, 0, NULL }
};
// 4 模块初始化方法
PyMODINIT_FUNC init_XunFei(void) {
  //初始模块，把MyCppMethods初始到MyCppModule中
  PyObject *m = Py_InitModule("_XunFei", MyCppMethods);
  if (m == NULL)
    return;
}


//5  /usr/bin/g++ -c -g -D_DEBUG -D_GNU_SOURCE -w -pthread -pipe -I/home/lxbin/Case/ROSNXTROBOT/src/xunfei_python/xunfei/../..///prj/../include -fPIC -o /home/lxbin/Case/ROSNXTROBOT/src/xunfei_python/xunfei/xebug/_XunFei.o /home/lxbin/Case/ROSNXTROBOT/src/xunfei_python/xunfei/_XunFei.cpp
//  /usr/bin/g++ -shared -Wl,-soname,_XunFei -g -D_DEBUG -D_GNU_SOURCE -w -pthread -pipe -I/home/lxbin/Case/ROSNXTROBOT/src/xunfei_python/xunfei/../../prj/../include -fPIC -o /home/lxbin/Case/ROSNXTROBOT/src/xunfei_python/xunfei/xebug/_XunFei /home/lxbin/Case/ROSNXTROBOT/src/xunfei_python/xunfei/xebug/_XunFei.o   -L/home/lxbin/Case/ROSNXTROBOT/src/xunfei_python/xunfei/../../prj/../bin -lmsc -ldl -lpthread -lrt


