#-*- coding: UTF-8 -*-
################################################################################
#
# File:         ROSNXT_CONSTANTS.py
# RCS:          $Header: $
# Description:  Constants for ROSNXT Control
# Author:       Xubin Li
# Created:      11 13 11:23:04 2014 (Shen Zhen University City) lxbin@uletian.cn
# Modified:     
# Language:     Python
# Package:      ROSNXT
# Status:       Experimental (Do Not Distribute)
#
# 
################################################################################
#
# Revisions:
#
################################################################################
#!/usr/bin/env python
PKG = 'NXTROS_XUNFEI_MOUSE' # this package name
import roslib
import rospy
from std_msgs.msg import String    
import threading


import os
import pyaudio
import wave
import sys
from datetime import datetime
from ROSNXT_CONSTANTS import *
from ROSNXT_XUNFEI import *
class Robot_MOUSE():
    def __init__(self,RECORD_SECONDS=0):
	rospy.init_node('Robot_MOUSE_ROSNXT', anonymous=True)
	self.talker = rospy.Publisher('ROSNXT_MOUSE', String)
	self.MouseRecInfo=""
	self.SYSRecInfo=""
    
	self.CHUNK = MOUSECHUNK
	self.FILENNAME =""
	if RECORD_SECONDS!=LOGININID:
		self.robotear=ROSNXT_XUNFEI(APPID0)
		ret=self.robotear.login()
		if ret==-1:
			print "迅飞服务器链接失败"
			self.status=False
		else:
			self.status=True		    
	else:
		self.status=False		    

    def run(self,WavFileName=MOUSESOUND):
    	self.FILENNAME=WavFileName
	self.wf = wave.open(self.FILENNAME, 'rb')
	self.p = pyaudio.PyAudio()
	self.stream = self.p.open(format=self.p.get_format_from_width(self.wf.getsampwidth()),
		        channels=self.wf.getnchannels(),
		        rate=self.wf.getframerate(),
		        output=True)
	self.data = self.wf.readframes(self.CHUNK)
	while self.data != '':
	    self.stream.write(self.data)
	    self.data = self.wf.readframes(self.CHUNK)
	    
    def text_to_speech(self,text):
    	if self.status==False:
		self.robotear=ROSNXT_XUNFEI(APPID0)
		ret=self.robotear.login()
		if ret==-1:
			print "迅飞服务器链接失败"
			self.status=False
		else:
			self.status=True
    	if self.status==True:
	    	self.ret = self.robotear.text_to_speech(text);
	    	print self.ret
	    	if self.ret==0:
	    		print "text_to_speech is Pass"
			self.run()
			self.exit_sound()
	    	else:
	    		print "text_to_speech is Failed"
	    		self.speaker(MOUSEERRSOUND)
	    		self.status=False
        return self.status
        
    def speaker(self,WavFileName):
        self.run(WavFileName)
        self.exit_sound()					
    def exit_sound(self):
	self.stream.stop_stream()
	self.stream.close()
	# close PyAudio (5)
	self.p.terminate()
	return 1
#=================================Roboert======================================	
    def callback(self,data):
    	rospy.loginfo("\nROSNXT_MOUSE heard: %s",data.data)
	self.MouseRecInfo=data.data
	print self.MouseRecInfo
	
    def system_callback(self,data):
    	rospy.loginfo("\nROSNXT SYSTEM heard: %s",data.data)
	self.SYSRecInfo=data.data
	print self.SYSRecInfo
        
    def ROSNXT_node_listener(self):
	self.listener=rospy.Subscriber('ROSNXT_MOUSE', String, self.callback)
	rospy.spin()
    def ROSNXT_sys_listener(self):
	self.listener=rospy.Subscriber('ROSNXT_SYS_STATUS', String, self.system_callback)
	rospy.spin()
    
    def ROSNXT_MOUSE_talker(self,data):
    	self.str = "MOUSE_%s"%data    	
    	self.talker.publish(self.str)
	
    def ROSNXT_SYS_statuts_talker(self,data):
    	global sys_status_pub	
    	self.str = "SYSTEM_%s"%data    	
    	sys_status_pub.publish(self.str)
        
def thread_main(a):
    global ROSNXT_MOUSE
    threadname = threading.currentThread().getName()
    if a==0:	
    	print "====================Start SYSTEM Listen Threading ================="
        ROSNXT_SYS_listener(ROSNXT_MOUSE)
    if a==1:
    	print "====================Wait NODE Listen Threading ======================"
        ROSNXT_NODE_listener(ROSNXT_MOUSE)
    if a==2:
    	print "====================Start Action Threading ========================="
        ROSNXT_SYS_Action(ROSNXT_MOUSE)
    if a==3:
    	print "====================Start Action Threading ========================="
        ROSNXT_NODE_Action(ROSNXT_MOUSE)
    	
    print threadname, a
def ROSNXT_SYS_listener(ROSNXT):
    ROSNXT.ROSNXT_sys_listener()
    return 

def ROSNXT_NODE_listener(ROSNXT):
    ROSNXT.ROSNXT_node_listener()
    return 
def ROSNXT_SYS_Action(ROSNXT):
   #rostopic pub /ROSNXT std_msgs/String "小智退出"
    #ROSNXT.ROSNXT_SYS_statuts_talker("开始")
    while(1):
	if "小智退出" in ROSNXT.SYSRecInfo:
		ROSNXT.ROSNXT_MOUSE_talker("退出")
		break		
	if "MOUSE故障" in ROSNXT.SYSRecInfo:
		ROSNXT.ROSNXT_SYS_statuts_talker("故障")
		ROSNXT.SYSRecInfo="MOUSE故障"
		continue
	if "MOUSE工作" in ROSNXT.SYSRecInfo:
		ROSNXT.ROSNXT_SYS_statuts_talker("EAR工作")
		continue
    return 
    
def ROSNXT_NODE_Action(ROSNXT):
   #rostopic pub /ROSNXT std_msgs/String "小智退出"      
    while(1):
	if "MOUSE_退出" in ROSNXT.MouseRecInfo:	
		break	
	if "系统休息中" in ROSNXT.MouseRecInfo:
		ROSNXT.MouseRecInfo="系统休息中"
		time.sleep(5)
		continue
	if "系统休息提示" in ROSNXT.MouseRecInfo:
		ROSNXT.text_to_speech("小智将休息了，请用手感应上方的传感器唤醒我，，，")
		ROSNXT.ROSNXT_MOUSE_talker("系统休息中")		
				
#============================ROSNXT_MOUSE===================================
	if "听命令提示" in ROSNXT.MouseRecInfo:
		ROSNXT.text_to_speech("命令：1.查询天气，2.开始摄像，3.语音导航 4.退出 ，请选择？")
		ROSNXT.ROSNXT_MOUSE_talker("命令控制提示结束")		
		continue
	if "语音导航" in ROSNXT.MouseRecInfo :
		ROSNXT.text_to_speech("语音控制命令：1.前进，2.后退，3.向左转，4.向右转,5退出 ，请选择？")
		ROSNXT.ROSNXT_MOUSE_talker("导航控制提示结束")
		continue	
#============================ROSNXT_MOUSE===================================		
	if "天气提示" in ROSNXT.MouseRecInfo:
		ROSNXT.text_to_speech("天气命令：1.本地天气，2.北京天气，3.南丰的天气 ，4.南昌的天气 5.退出，请选择？")
		ROSNXT.ROSNXT_MOUSE_talker("天气控制提示结束")
		continue
	if "播报天气" in ROSNXT.MouseRecInfo:
		ROSNXT.text_to_speech(ROSNXT.MouseRecInfo[ROSNXT.MouseRecInfo.index('播报天气'):])
		ROSNXT.ROSNXT_MOUSE_talker("天气播报结束")
		continue
#============================ROSNXT_MOUSE===================================		
	if "摄像提示" in ROSNXT.MouseRecInfo:
		ROSNXT.text_to_speech("摄像控制命令：1.准备拍照，2.看照片，3.退出，请选择？")
		ROSNXT.ROSNXT_MOUSE_talker("摄像控制提示结束")
		continue
	if "拍照提示" in ROSNXT.MouseRecInfo:
		ROSNXT.text_to_speech("拍照控制命令：1.开始拍照，2.退出，请选择？")
		ROSNXT.ROSNXT_MOUSE_talker("拍照控制提示结束")
		continue
	if "看照片提示" in ROSNXT.MouseRecInfo:
		ROSNXT.text_to_speech("看照片：1.第1张照片，2.下一张，3.上一张 ，4.退出，请选择？")
		ROSNXT.ROSNXT_MOUSE_talker("看照片控制提示结束")
		continue
#============================ROSNXT_MOUSE===================================		

	if "失败" in ROSNXT.MouseRecInfo:
		ROSNXT.text_to_speech(ROSNXT.MouseRecInfo)
#		ROSNXT.ROSNXT_MOUSE_talker(ROSNXT.MouseRecInfo)
#		ROSNXT.ROSNXT_MOUSE_talker("失败播报结束")
		continue	
		
    return 
	
def test():
    print "Please send your command"	
    ROSNXT_MOUSE=Robot_MOUSE()
    for i in range(2):
	    ROSNXT_MOUSE.speaker("./text_to_speech.wav")
	    ROSNXT_MOUSE.text_to_speech("北京天气多少度")
	    print "=======***************************====time ",i,"============*************************======="
    exit(0)                      
 	
if __name__ == '__main__':      
#   test()
	#rostopic pub /ROSNXT std_msgs/String "bbbbb"    
	#test()  
 	#exit(0)
 	global ROSNXT_MOUSE,sys_status_pub
 	sys_status_pub=rospy.Publisher('ROSNXT_SYS_STATUS', String)
 	
 	threads = [] 
 	ROSNXT_MOUSE=Robot_MOUSE(LOGININID)
 	for x in xrange(0, 4):
        	threads.append(threading.Thread(target=thread_main, args=(x,)))
    	for t in threads:
        	t.start()
    	for t in threads:
        	t.join()
 	threads = []
	exit(0)	    
    	
